(function(){

	$(document).ready(function(){

		$('.carrossel-planta').owlCarousel({
			items: 1,
			dots: true,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			smartSpeed: 450,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,
			// 		margin: 15,
			// 	},
			// 	425:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	500:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	768:{
			// 		items:3,
			// 		margin: 30,
			// 	},
			// 	850:{
			// 		items:4,
			// 	},
			// }
		});

		$('.carrossel-diferenciais').owlCarousel({
			items: 1,
			dots: false,
			nav: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,
			// 		margin: 15,
			// 	},
			// 	425:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	500:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	768:{
			// 		items:3,
			// 		margin: 30,
			// 	},
			// 	850:{
			// 		items:4,
			// 	},
			// }
		});

		$('.carrossel-estagios-obra').owlCarousel({
			items: 1,
			dots: false,
			nav: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,
			// 		margin: 15,
			// 	},
			// 	425:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	500:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	768:{
			// 		items:3,
			// 		margin: 30,
			// 	},
			// 	850:{
			// 		items:4,
			// 	},
			// }
		});

	});

}());